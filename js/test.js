jQuery( function( $ ) {
    // STAGE
    var stage = new $.Stage( $( "section#wrapper-game" ) );
    var floor = new $.ObjGame( $( "div#floor" ) );
    var floor2 = new $.ObjGame( $( "div#floor2" ) );
    var tree = new $.ObjGame( $( "div#tree" ) );
    var bird = new $.Bird( $( "div#bird" ), 3 );

    // PLAYER
    var playerTest = new $.Player( $( "div#playerTest" ), 8 );
    var oxygenPlayer = new $.Armor( $( "div#oxygen.player" ), 1 );

    // ITEMS
    var oxygen1 = new $.Fx( $( "div#oxygen.oxygen-1" ), 1 );
    var oxygen2 = new $.Fx( $( "div#oxygen.oxygen-2" ), 1 );
    var medal1 = new $.Fx( $( "div#medals.medal-1"), 9 )

    // TARGETS
    var radiactivo1 = new $.Fx( $( "div#radiactivo.one" ), 9 );
    var radiactivo2 = new $.Fx( $( "div#radiactivo.two" ), 9 );
    var gunMetal = $( "div#gun-metal" );
    var ballMetal = new $.Fx( $( "div#ball-metal" ), 4 );
    var acid = new $.Fx( $( "div#acid" ), 9);
    var blurAcid = new $.Fx( $( "div#blur-acid" ), 9);
    var robot = new $.Fx( $( "div#robot-boss" ), 7 );
    var fireGun = new $.Fx( $( "div#fire-supergun" ), 13 );
    var cohete = new $.Fx( $( "div#cohete" ), 5 );

    // GUI
    var startMenu = $( "div#start-menu" );
    var gameOver = $( "div#gameover" );
    var countLife = new $.Count( $( "div#gui-count-player-life" ) );
    var countOxygen = new $.Count( $( "div#gui-count-oxygen-munition" ) );

    // LINKS
    var linkGuadua = $( "a#link-guadua" );
    var linkFlora = $( "a#link-flora" );
    var linkFosil = $( "a#link-fosil" );

    // CONTROLLERS
    InitGame = false;
    InitShoot = oxygenPlayer.InitShoot;

    $( document ).keydown( function( event ) {
        switch( event.keyCode ) {
            case 13:
                if ( !InitGame ) {
                    startMenu.hide( 'fast' );
                    InitGame = true;
                    StartGame();
                } else {
                    return( false );
                }
            break;
            case 32:
                if( InitGame ) {
                    oxygenPlayer.shoot();
                    playerTest.shoot();
                    InitShoot = oxygenPlayer.InitShoot;
                } else {
                    return( false );
                }
        }
    });

// HIDE OBJECTS
//
    // TARGETS
    acid.hideObject();
    blurAcid.hideObject();
    robot.hideObject();
    cohete.hideObject();
    fireGun.hideObject();
    $( "div#gun-boss" ).hide();

    // ITEMS
    oxygen1.hideObject();
    oxygen2.hideObject();
    medal1.hideObject();

    // PLAYER
    oxygenPlayer.InitEvents();

    // GUI
    gameOver.hide();


function StartGame() {
    if ( !InitGame ) {
        return( false );
    } else {
// INIT EVENTS
//
    // STAGE
    stage.InitEvents();
    floor.InitEvents();
    floor2.InitEvents();
    tree.InitEvents();

    // PLAYER
    playerTest.InitEvents();

    // ITEMS
    oxygen1.InitEvents();
    oxygen2.InitEvents();
    medal1.InitEvents();

    // TARGETS
    radiactivo1.InitEvents();
    radiactivo2.InitEvents();
    ballMetal.InitEvents();
    acid.InitEvents();
    blurAcid.InitEvents();
    robot.InitEvents();
    fireGun.InitEvents();
    cohete.InitEvents();

    // GUI
    countLife.InitEvents();
    countOxygen.InitEvents();
//

// INTERVAL FUNCTIONS
//
    // COLLISION
    setInterval( function() {

        if ( !InitShoot ) {
            oxygenPlayer.element.css( { 'left' : playerTest.getRight() +'px', 'top' : ( playerTest.getTop() + 75 - 10 ) +'px' } );
        } else {
            if ( oxygenPlayer.getLeft() > 1024 ) {
                oxygenPlayer.hideObject();
                oxygenPlayer.element.css( { 'left' : playerTest.getRight() +'px', 'top' : ( playerTest.getTop() + 75 - 10 ) +'px' } );
                InitShoot = false;
            }
        }

        // DEFAULT ACTION DIE
        if ( playerTest.Life <= 0 ) {
            playerTest.setVelocity( 0 );
            InitGame = false;
            gameOver.show();
        }

        // COLLISION
        if ( playerTest.IntersectBottom( floor ) && !playerTest.IntersectLeft( floor ) || playerTest.IntersectBottom( floor2 ) && playerTest.getRight() > floor2.getLeft() + 20 ) {
            playerTest.element.css( 'top', floor.getTop() - playerTest.Height() +'px' );

            // START OBJECT LVL 2
            if ( playerTest.getRight() >= 1024 && stage.getLevel() == 1 ) {

                // PLAYER
                playerTest.startPositionDefault();

                // ITEMS
                oxygen1.showObject();
                oxygen2.showObject();
                medal1.showObject();

                // TARGETS
                radiactivo1.positionOff();
                radiactivo2.positionOff();
                acid.showObject();
                blurAcid.showObject();
                gunMetal.hide();
                ballMetal.hideObject();

                //STAGE
                bird.InitEvents();
                tree.hideObject();
                floor2.hideObject();
                floor.element.width( 1024 );
                stage.InitLevel_2();

            // START OBJECT LVL 3
            } else if ( playerTest.getRight() >= 1024 && stage.getLevel() == 2 ) {
                if ( !stage.timeOut ) {

                    // PLAYER
                    playerTest.startPositionDefault();

                    // ITEMS
                    oxygen1.hideObject();
                    oxygen2.hideObject();
                    medal1.hideObject();

                    // TARGETS
                    acid.positionOff();
                    blurAcid.positionOff();
                    robot.showObject();
                    fireGun.showObject();
                    cohete.showObject();
                    $( "div#gun-boss" ).show();

                    // STAGE
                    stage.InitLevel_3();
                } else {
                    stage.InitLevel_2();
                    stage.timeOut = false;
                }
            }

        } else if ( playerTest.IntersectBottom( tree ) && playerTest.getRight() > tree.getLeft() + 20 && playerTest.getLeft() < tree.getRight() - 20 && stage.getLevel() == 1 ) {
            playerTest.element.css( 'top', tree.getTop() - playerTest.Height() +'px' );
        } else if ( playerTest.getLeft() > floor.getRight() - 20 && playerTest.getRight() < tree.getLeft() + 20 && playerTest.getBottom() >= floor.getTop() || playerTest.getLeft() > tree.getRight() - 20 && playerTest.getRight() < floor2.getLeft() + 20 && playerTest.getBottom() >= floor2.getTop() ) {
            if ( stage.getLevel() == 1 ) {
                playerTest.element.css( {
                    'top' : '+='+ 10,
                    'left' : playerTest.getLeft() +'px'
                } );
                playerTest.setVelocity( 0 );
            }
            if ( playerTest.getTop() >= 576 ) {
                playerTest.Die();
                playerTest.setVelocity( 5 );
            }
        } else if ( !playerTest.IntersectBottom( floor ) || !playerTest.IntersectBottom( tree ) || !playerTest.IntersectBottom( floor2 ) ) {
            playerTest.element.css( { 'top' : '+='+ 10 } );
        }
    }, 40 );

    // INTERACTIVE OBJECTS
    setInterval( function() {

        // STAGE 1
        if ( stage.getLevel() == 1 ) {
            linkGuadua.show();

            // RADIACTIVE TARGETS INTERACTIVE
            if ( playerTest.TouchMe( radiactivo1 ) || playerTest.TouchMe( radiactivo2 ) ) {
                playerTest.Die();
            }

            if ( playerTest.TouchMeBall( ballMetal ) ) {
                playerTest.Die();
            }

        // STAGE 2
        } else if ( stage.getLevel() == 2 ) {
            linkGuadua.hide();
            linkFlora.show();

            // ACID TARGETS INTERACTIVE
            if ( playerTest.TouchSome( acid, 50, 40 ) ) {
                // SETTINGS IF LIFE IS 1
                if ( playerTest.Life == 1 ) {
                    if ( !playerTest.timeOut ) {

                        // PLAYER
                        playerTest.Die();

                        // TARGETS
                        radiactivo1.InitEvents();
                        radiactivo1.showObject();
                        radiactivo2.InitEvents();
                        radiactivo2.showObject();
                        acid.positionOff();

                        // STAGE
                        stage.InitLevel_1();
                    }

                // SETTINGS IF LIFE IS 2
                } else if( playerTest.Life == 2 ) {

                    // PLAYER
                    playerTest.Die();
                }
            } else {
                playerTest.timeOut = false;
            }

            // MEDAL ITEM INTERACTIVE
            if ( playerTest.TouchMe( medal1 ) ) {

                // SETTINGS IF LIFE IS 1 OR 2
                if ( playerTest.Life != 0 && playerTest.Life <= 2 ) {

                    // SETTINGS IF LIFE IS 1
                    if ( playerTest.Life == 1 ) {
                        playerTest.newLife();
                    } else {
                        return( false );
                    }
                }
                medal1.positionOff();
            }

            // MEDAL ITEM INTERACTIVE
            if ( playerTest.TouchMe( oxygen1 ) ) {
                if ( playerTest.oxygenMunition == 0 ) {
                    playerTest.oxygenMunition = 3;
                    oxygenPlayer.charge( playerTest.oxygenMunition );
                } else if ( playerTest.oxygenMunition == 3 ) {
                    playerTest.oxygenMunition = 6;
                    oxygenPlayer.charge( playerTest.oxygenMunition );
                } else {
                    return( false );
                }
                oxygen1.positionOff();
            } else if ( playerTest.TouchMe( oxygen2 ) ) {
                if ( playerTest.oxygenMunition == 0 ) {
                    playerTest.oxygenMunition = 3;
                    oxygenPlayer.charge( playerTest.oxygenMunition );
                } else if ( playerTest.oxygenMunition == 3 ) {
                    playerTest.oxygenMunition = 6;
                    oxygenPlayer.charge( playerTest.oxygenMunition );
                } else {
                    return( false );
                }
                oxygen2.positionOff();
            }

        // STAGE 3
        } else if ( stage.getLevel() == 3 ) {
                linkFlora.hide();
                linkFosil.show();
            if ( playerTest.TouchMe( robot ) ) {
                console.info( 'tocando robot' );
            }
        }

        // GUI OXYGEN MUNITION
        switch( playerTest.oxygenMunition ) {
            case 0:
                countOxygen.Cero();
            break;
            case 1:
                countOxygen.One();
            break;
            case 2:
                countOxygen.Two();
            break;
            case 3:
                countOxygen.Three();
            break;
            case 4:
                countOxygen.Four();
            break;
            case 5:
                countOxygen.Five();
            break;
            case 6:
                countOxygen.Six();
            break;
        }

        // GUI PLAYER LIFE
        switch( playerTest.Life ) {
            case 0:
                countLife.Cero();
            break;
            case 1:
                countLife.One();
            break;
            case 2:
                countLife.Two();
            break;
        }

    }, 30 );


    var $BallMetal = $( "div#ball-metal" );

    gunMetal.ready( function() {
        setInterval( function() {
            gunMetal.animate( { 'top' : '-100px' }, 950 ).animate( { 'top' : '-30px' }, 950 );
        }, 950 );
    } );

    $BallMetal.ready( function() {

        var frames = 4;
        var xFrame = 0;
        var nextXFrame = 60;

        setInterval( function() {
            var gunTop = parseInt( gunMetal.position().top );
            var x = xFrame + nextXFrame;
            $BallMetal.animate( { 'top' : '-20px' }, 0 );
            if ( gunTop <= -30 && gunTop >= -35 ) {
                $BallMetal.css( { 'background-position' : '-'+ x +'px 0px' } ).animate( { 'top' : '600px' }, 5000 );
            }
            $BallMetal.css( { 'background-position' : '-'+ x +'px 0px' } );
            xFrame = x;
            if ( x >= frames * nextXFrame ) xFrame = 0;
        }, 200 );
    } );

    }
}

} );