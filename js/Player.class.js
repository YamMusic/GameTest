jQuery( function( $ ) {

	$.Player = function( element, frames ) {
		this.element = ( element instanceof $ ) ? element : $( element );
		this.frames = frames;
		this.direction;
		this.faceDirection = 'right';
		this.Left;
		this.Top;
		this.Right;
		this.Bottom;
		this.width;
		this.height;
		this.velocity;
		this.velocityDown;
		this.Life;
		this.oxygenMunition;
		this.timeOut;
	};

	$.Player.prototype = {

		InitEvents : function() {
			this.InitDefault();
			this.InitPlayer();
			var that = this;

			$( document ).keydown( function( event ) {
				var keydownCode = event.which;
				// event.preventDefault();

				switch ( keydownCode ) {
					case 37:
						that.moveLeft();
					break;
					case 38:
						that.moveTop();
					break;
					case 39:
						that.moveRight();
					break;
					case 32:
						that.shootOxygen();
					break;
				}
			} );
		},

		InitDefault : function() {
			// SET X AND Y POSITION
			setInterval( ( function() {
				this.setPosition(
					this.element.position().left,
					this.element.position().top
				);
			} ).bind( this ), 30 );
		},

		setPosition : function( left, top ) {
			this.Left = left;
			this.Top = top;
			this.Right = left + this.element.width();
			this.Bottom = top + this.element.height();
			this.width = this.element.width();
			this.height = this.element.height();
		},

		startPositionDefault : function() {
			this.element.css( { 'top' : '235px', 'left' : '30px' } );
		},

		Width : function() {
			return this.width;
		},

		setWidth : function( width ) {
			this.width = width;
		},

		Height : function() {
			return this.height;
		},

		setHeight : function( height ) {
			this.height = height;
		},

		getLeft : function() {
			return this.Left;
		},

		getTop : function() {
			return this.Top;
		},

		getRight : function() {
			return this.Right;
		},

		getBottom : function() {
			return this.Bottom;
		},

		InitPlayer : function() {
			this.Life = 1;
			this.oxygenMunition = 0;
			var that = this;
			that.setVelocity( 5 );
			that.setVelocityDown( 5 );

			this.element.ready( function() {
				var frameX = 0;

				setInterval( ( function() {
					var x = frameX + that.element.width();
					if ( that.direction == 'right' ) {
						that.element.removeClass( 'direction-left' );
						that.element.css( { "background-position" : "-"+ x +"px 0px" } );
					} else if ( that.direction == 'left' ) {
						that.element.addClass( 'direction-left' );
						that.element.css( {
							"background-position" : "-"+ x +"px 0px"
						} );
					} else if ( that.direction == null ) {
						that.element.css( { "background-position" : "0px 0px" } );
					}
					frameX = x;
					if ( x >= ( that.frames - 1 ) * that.element.width() ) frameX = 0;
					that.direction = null;
				} ).bind( this ), 170 );

			} );
		},

		setVelocity : function( velocity ) {
			this.velocity = velocity;
		},

		getVelocity : function() {
			return this.velocity;
		},

		setVelocityDown : function( velocityDown ) {
			this.velocityDown = velocityDown;
		},

		getVelocityDown : function() {
			return this.velocityDown;
		},

		moveLeft : function() {
			this.direction = 'left';
			this.faceDirection = 'left';
			var left = this.element.position().left;
			if ( left <= 0 ) {
				this.element.css( { "left" : '0px' } );
			} else {
				this.element.css( { "left" : "-="+ this.getVelocity() } );
			}
		},

		moveTop : function() {
			var that = this;
			this.direction = 'top';
			var top = this.element.position().top;
			if ( top <= 0 ) {
				this.element.css( { "top" : '0px' } );
			} else {
				if ( this.faceDirection == 'left' ) {
					this.element.animate( { 'top' : '-='+ 90, "left" : "-="+ 130 }, 300 );
					// this.element.animate( { "top" : "+="+ 80 }, 300 );
				} else {
					this.element.animate( { 'top' : '-='+ 90, "left" : "+="+ 130 }, 300 );
					// this.element.animate( { "top" : "+="+ 80 }, 300 );
				}
			}
		},

		moveRight : function() {
			this.direction = 'right';
			this.faceDirection = 'right';
			var left = this.element.position().left;
			// if ( left >= 960 ) {
			// 	this.element.css( {
			// 		"left" : ( this.element.parent().width() - this.element.width() ) +'px'
			// 	} );
			// } else{
				this.element.css( { "left" : "+=" + this.getVelocity() } );
			// }
		},

		shootOxygen : function() {
			this.element.css( { 'background-position' : '-'+ ( this.element.width() * 8 ) +'px 0px' } );
		},

		Intersects : function( obj ) {
			if ( this.getLeft() > obj.getRight() )
				return( false );
			else if ( this.getRight() < obj.getLeft() )
				return( false );
			else if ( this.getTop > obj.getBottom() )
				return( false );
			else if ( this.getBottom() < obj.getTop() )
				return( false );
			else
				return( true );
		},

		IntersectBottom : function( obj ) {
			if ( this.getBottom() >= obj.getTop() ) return( true );
			else return( false );
		},

		IntersectLeft : function( obj ) {
			if ( this.getLeft() >= obj.getRight() - 20 ) return( true );
			else return( false );
		},

		IntersectRight : function( obj ) {
			if ( this.getRight() >= obj.getLeft() - 5 ) return( true );
			else return( false );
		},

		TouchMe : function( obj ) {
			if ( this.getRight() >= (obj.getLeft() + 20) && this.getRight() <= (obj.getRight() + 20) && this.getBottom() >= obj.getTop() || this.getLeft() >= obj.getLeft() && this.getLeft() <= (obj.getRight() - 20) && this.getBottom() >= obj.getTop() ) return( true );
			else return( false );
		},

		TouchMeBall: function( obj ) {
			if ( this.getRight() >= (obj.getLeft() + 20) && this.getRight() <= (obj.getRight() + 20) && this.getBottom() >= obj.getTop() && this.getTop() <= obj.getBottom() || this.getLeft() >= obj.getLeft() && this.getLeft() <= (obj.getRight() - 20) && this.getBottom() >= obj.getTop() && this.getTop() <= obj.getBottom() ) return( true );
			else return( false );
		},

		TouchSome : function( obj, distance, height ) {
			if ( this.getRight() >= (obj.getLeft() + distance ) && this.getRight() <= (obj.getRight() - distance ) && this.getBottom() >= ( obj.getTop() + height ) || this.getLeft() >= obj.getLeft() && this.getLeft() <= (obj.getRight() - distance ) && this.getBottom() >= ( obj.getTop() +  height ) ) return( true );
			else return( false );
		},

		Die : function() {
			this.element.hide();
			this.startPositionDefault();
			if ( !this.timeOut ) {
				if ( this.Life == 1 ) {
					this.Life = 0;
					this.timeOut = true;
				} else if ( this.Life == 2 ) {
					this.Life = 1;
					this.timeOut = true;
				} else if ( this.Life == 0 ) {
					this.Life = 0;
					this.setVelocity( 0 );
				}
			}
			this.element.show();
		},

		newLife : function() {
			this.Life++;
		},

		loseLife : function() {
			this.Life--;
		},

		shoot : function() {
			this.oxygenMunition--;
		}

	};

} );