jQuery( function( $ ) {

	$.Fx = function( element, frames ) {
		this.element = ( element instanceof $ ) ? element : $( element );
		this.frames = frames;
		this.Left;
		this.Top;
		this.Right;
		this.Bottom;
		this.width;
		this.height;
		this.initPositionX;
		this.initPositionY;
	};

	$.Fx.prototype = {

		InitEvents : function() {
			this.InitDefault();
			this.InitFx();
		},

		setInitPosition : function( x , y ) {
			this.initPositionX = x;
			this.initPositionY = y;
			this.initPosition();
		},

		initPosition : function() {
			this.element.show();
			this.element.css( { 'top' : this.initPositionY +'px', 'left' : this.initPositionX +'px' } );
		},

		positionOff : function() {
			this.element.hide();
			this.element.css( { 'top' : '0px', 'left' : '-1024px' } );
		},

		InitDefault : function() {
			// SET X AND Y POSITION
			var that = this;
			setInterval( function() {
				that.setPosition( that.element.position().left, that.element.position().top );
			}, 30 );
		},

		setPosition : function( left, top ) {
			this.Left = left;
			this.Top = top;
			this.Right = left + this.element.width();
			this.Bottom = top + this.element.height();
			this.width = this.element.width();
			this.height = this.element.height();
		},

		Width : function() {
			return this.width;
		},

		Height : function() {
			return this.height;
		},

		getLeft : function() {
			return this.Left;
		},

		getTop : function() {
			return this.Top;
		},

		getRight : function() {
			return this.Right;
		},

		getBottom : function() {
			return this.Bottom;
		},

		InitFx : function() {
			var that = this;

			this.element.ready( function() {
				var frameX = 0;

				setInterval( ( function() {
					var x = frameX + that.element.width();
					that.element.css( { "background-position" : "-"+ x +"px 0px" } );
					frameX = x;
					if ( x >= that.frames * that.element.width() )
						frameX = 0;
				} ).bind( this ), 120 );

			} );
		},

		hideObject : function() {
            this.element.hide();
        },

        showObject : function() {
            this.element.show();
        }

	};
});