jQuery( function( $ ) {

	$.Armor = function( element, frames ) {
		this.element = ( element instanceof $ ) ? element : $( element );
		this.frames = frames;
		this.Left;
		this.Top;
		this.Right;
		this.Bottom;
		this.width;
		this.height;
		this.initPositionX;
		this.initPositionY;
		this.munition;
		this.InitShoot = false;
	};

	$.Armor.prototype = {

		InitEvents : function() {
			this.InitDefault();
			this.InitArmor();
		},

		setInitPosition : function( x , y ) {
			this.initPositionX = x;
			this.initPositionY = y;
			this.initPosition();
		},

		initPosition : function() {
			this.element.show();
			this.element.css( { 'top' : this.initPositionY +'px', 'left' : this.initPositionX +'px' } );
		},

		positionOff : function() {
			this.element.hide();
			this.element.css( { 'top' : '0px', 'left' : '-1024px' } );
		},

		InitDefault : function() {
			// SET X AND Y POSITION
			var that = this;
			setInterval( function() {
				that.setPosition( that.element.position().left, that.element.position().top );
			}, 30 );
		},

		setPosition : function( left, top ) {
			this.Left = left;
			this.Top = top;
			this.Right = left + this.element.width();
			this.Bottom = top + this.element.height();
			this.width = this.element.width();
			this.height = this.element.height();
		},

		Width : function() {
			return this.width;
		},

		Height : function() {
			return this.height;
		},

		getLeft : function() {
			return this.Left;
		},

		getTop : function() {
			return this.Top;
		},

		getRight : function() {
			return this.Right;
		},

		getBottom : function() {
			return this.Bottom;
		},

		InitArmor : function() {
			this.munition = 0;
			this.hideObject();
		},

		hideObject : function() {
            this.element.hide();
        },

        showObject : function() {
            this.element.show();
        },

        TouchMe : function( obj ) {
			if ( this.getRight() >= (obj.getLeft() + 20) && this.getRight() <= (obj.getRight() + 20) && this.getBottom() >= obj.getTop() && this.getTop() <= obj.getBottom() || this.getLeft() >= obj.getLeft() && this.getLeft() <= (obj.getRight() - 20) && this.getBottom() >= obj.getTop() && this.getTop() <= obj.getBottom() ) return( true );
			else return( false );
		},

		charge : function( munition ) {
			this.munition = munition;
		},

		shoot : function() {
			if ( this.munition == 0 ) {
				return( false );
			} else {
				this.showObject();
				this.element.animate( { 'left' : '1030px' }, 900 );
				this.munition--;
				console.info( this.munition );
				this.InitShoot = true;
			}
		}

	};
});