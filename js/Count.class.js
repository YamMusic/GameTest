jQuery( function( $ ) {

	$.Count = function( element ) {
		this.element = ( element instanceof $ ) ? element : $( element );
		this.Left;
		this.Top;
		this.Right;
		this.Bottom;
		this.width;
		this.height;
		this.maxCount = 2;
		this.count;
	};

	$.Count.prototype = {

		InitEvents : function() {
			this.InitDefault();
			this.InitCount();
		},

		InitDefault : function() {
			// SET X AND Y POSITION
			this.setPosition( this.element.position().left, this.element.position().top );
		},

		setPosition : function( left, top ) {
			this.Left = left;
			this.Top = top;
			this.Right = left + this.element.width();
			this.Bottom = top + this.element.height();
			this.width = this.element.width();
			this.height = this.element.height();
		},

		Width : function() {
			return this.width;
		},

		Height : function() {
			return this.height;
		},

		getLeft : function() {
			return this.Left;
		},

		getTop : function() {
			return this.Top;
		},

		getRight : function() {
			return this.Right;
		},

		getBottom : function() {
			return this.Bottom;
		},

		InitCount : function() {
			this.One();
		},

		Cero : function() {
			this.element.css( { 'background-position' : '0px 0px' } );
		},

		One : function() {
			this.element.css( { 'background-position' : '-'+ this.Width() +'px 0px' } );
		},

		Two : function() {
			this.element.css( { 'background-position' : '-'+ ( this.Width() * 2 ) +'px 0px' } );
		},

		Three : function() {
			this.element.css( { 'background-position' : '-'+ ( this.Width() * 3 ) +'px 0px' } );
		},

		Four : function() {
			this.element.css( { 'background-position' : '-'+ ( this.Width() * 4 ) +'px 0px' } );
		},

		Five : function() {
			this.element.css( { 'background-position' : '-'+ ( this.Width() * 5 ) +'px 0px' } );
		},

		Six : function() {
			this.element.css( { 'background-position' : '-'+ ( this.Width() * 6 ) +'px 0px' } );
		},

		hideCount : function() {
            this.element.hide();
        },

        showCount : function() {
            this.element.show();
        }

	};
});