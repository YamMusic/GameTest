jQuery( function( $ ) {

	$.Stage = function( element ) {
		this.element = element;
		this.width;
		this.height;
		this.level;
		this.life;
		this.timeOut;
	};

	$.Stage.prototype = {

		InitEvents : function() {
			this.setLevel( 1 );
			this.InitDefault();
			this.InitStage();
		},

		InitDefault : function() {
			// SET X AND Y POSITION
			this.setPosition();
		},

		InitLevel_1 : function() {
			this.setLevel( 1 );
			this.element.css( { "background-position" : "0px 0px" } );
			this.timeOut = true;
		},

		InitLevel_2 : function() {
			this.setLevel( 2 );
			this.element.css( { "background-position" : "-1024px 0px" } );
			this.timeOut = true;
		},

		InitLevel_3 : function() {
			this.setLevel( 3 );
			this.element.css( { "background-position" : "-2048px 0px" } );
			this.timeOut = true;
		},

		setPosition : function() {
			this.width = this.element.width();
			this.height = this.element.height();
		},

		Width : function() {
			return this.width;
		},

		Height : function() {
			return this.height;
		},

		InitStage : function() {
			// this.element.css( { "background-position" : "0px 0px" } );
			this.InitLevel_1();
		},

		setLevel : function( level ) {
			this.level = level;
		},

		getLevel : function() {
			return this.level;
		}

	};

});