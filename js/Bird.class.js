jQuery( function( $ ) {

	$.Bird = function( element, frames ) {
		this.element = ( element instanceof $ ) ? element : $( element );
		this.frames = frames;
		this.Left;
		this.Top;
		this.Right;
		this.Bottom;
		this.width;
		this.height;
		this.velocity;
	};

	$.Bird.prototype = {

		InitEvents : function() {
			this.InitDefault();
			this.InitBird();
		},

		InitDefault : function() {
			// SET X AND Y POSITION
			setInterval( ( function() {
				this.setPosition(
					this.element.position().left,
					this.element.position().top
				);
			} ).bind( this ), 10 );
		},

		setPosition : function( left, top ) {
			this.Left = left;
			this.Top = top;
			this.Right = left + this.element.width();
			this.Bottom = top + this.element.height();
			this.width = this.element.width();
			this.height = this.element.height();
		},

		Width : function() {
			return this.width;
		},

		Height : function() {
			return this.height;
		},

		getLeft : function() {
			return this.Left;
		},

		getTop : function() {
			return this.Top;
		},

		getRight : function() {
			return this.Right;
		},

		getBottom : function() {
			return this.Bottom;
		},

		InitBird : function() {
			var that = this;
			that.setVelocity( 20 );

			this.element.ready( function() {
				var frameX = 0;

				setInterval( ( function() {
					var x = frameX + that.element.width();
					if ( that.getLeft() < ( 0 - that.Width() ) ) {
						that.element.css( { "background-position" : "0px 0px" } );
					} else {
						that.element.css( { "left" : "-="+ that.getVelocity() } );
						that.element.css( { "background-position" : "-"+ x +"px 0px" } );
					}
					frameX = x;
					if ( x >= that.frames * that.element.width() )
						frameX = 0;
				} ).bind( this ), 170 );

			} );
		},

		setVelocity : function( velocity ) {
			this.velocity = velocity;
		},

		getVelocity : function() {
			return this.velocity;
		}

	};
});